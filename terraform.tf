provider "aws" {
   region = "us-east-1"
}

resource "aws_vpc" "my_vpc" {
   cidr_block       = "10.0.0.0/16"
   
  tags = {
    Name = "main"
  }
}

resource "aws_subnet" "my_subnet" {
   vpc_id     = aws_vpc.my_vpc.id
   cidr_block = "10.0.1.0/24"
   availability_zone = "us-east-1a"
  tags = {
    Name = "Main"
  }
}

resource "aws_security_group" "my_securitygroup" {
  vpc_id = aws_vpc.my_vpc.id
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_instance" "my_instance" {
  ami = "ami-0440d3b780d96b29d"
  instance_type = "t2.medium"
  key_name =   "sahilec2key"
  subnet_id = aws_subnet.my_subnet.id

  tags={
    Name = "my_instance"
  }
}
