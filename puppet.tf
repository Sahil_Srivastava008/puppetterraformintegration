provider "aws" {
  region = "us-east-1"
}

data "aws_vpc" "existing_vpc" {
  id = "vpc-0ab6dcbc19bb2b76f"
}

data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-07d08a3f83b5deab1"
}

data "aws_security_group" "existing_securitygroup" {
  id = "sg-0ff93f22060a7c645"
}

resource "aws_instance" "puppetMaster" {
  ami                         = "ami-0440d3b780d96b29d"
  instance_type               = "t2.medium"
  subnet_id                   = data.aws_subnet.existing_subnet.id
  key_name                    = "sahilec2key"
  associate_public_ip_address = true
  security_groups             = [data.aws_security_group.existing_securitygroup.id]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:\\Users\\sriva\\Downloads\\sahilec2key.pem")
    host        = aws_instance.puppetMaster.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}
